(function($){
  
    var script = document.createElement("SCRIPT");
    script.src = 'https://www.googletagmanager.com/gtag/js?id=UA-144340552-1';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'UA-144340552-1');

    $.ajax({
        crossOrigin: true,
        url: "https://accounts.muslimapp.id/p/widgets/banner/api",
        type:"GET",
        accept: "application/json",
        contentType : "application/x-www-form-urlencoded; charset=UTF-8",
        success: function(data){
          var userAgent = navigator.userAgent || navigator.vendor || window.opera;
          
          // Windows Phone must come first because its UA also contains "Android"
          if (/windows phone/i.test(userAgent)) {
              return "Windows Phone";
          }

          if (/android/i.test(userAgent)) {
              return "Android";
          }

          // iOS detection from: http://stackoverflow.com/a/9039885/177710
          if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
              return "iOS";
          }

          var html = ""

          if (data.garis == 'Vertical') {
            html += '<div class="padding-row">'
            html += '<div class="vertical-img">'
            html += '<a id="hrefImg" data="'+userAgent+'" data-csrf="'+data.csrf_token+'">'
            html += '<img src="'+data.image+'" title="'+data.title+'" class="img-vertical" style="width:100%"></img>'
            html += '</a>'
            html += '</div>'
            html += '</div>'
          } else {
            html += '<div class="padding-row">'
            html += '<div class="horizontal-img"></div>'
            html += '<a id="hrefImg" data="'+userAgent+'" data-csrf="'+data.csrf_token+'">'
            html += '<img src="'+data.image+'" title="'+data.title+'" class="img-horizontal" style="width:100%"></img>'
            html += '</a>'
            html += '</div>'
            html += '</div>'
          }

          $("#bannerMuslimapp").html(html);
        }
    });

    $(document).on("click","#hrefImg", function() {
      $.ajax({
        url         : "https://accounts.muslimapp.id/p/widgets/banner/api/getHref",
        type        : "GET",
        success: function(data){
          $("#hrefImg").attr('href',data);
          window.location.href=data
        }
      })
    });

    $.ajax({
      url: "https://accounts.muslimapp.id/p/widgets/video/api",
      type:"get",
      accept: "application/json",
      contentType : "application/x-www-form-urlencoded; charset=UTF-8",
      success: function(data){
        var html = ''
        html +='<div class="container">'
        html +='<iframe width="560" height="315" src="'+data.url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        html += '</div>'
        $("#videoPublisher").html(html);
      }
    });

})(jQuery);