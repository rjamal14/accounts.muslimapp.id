<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->hostapi2  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->proapi   = "https://pro.muslimapp.id";
    }

    function saveRequest(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        
        $amount     = $request->input('amount');
        $nota       = $request->input('nota');
        
        try {
            $response    = $http->post($this->devapi.'/Wallet/Request', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
                'json' => [
                        'user_id'   => 1, //$user['user_id'],
                        'amount'    => $amount,
                ]
            ]);
    
            $get_response       = json_decode((string) $response->getBody(), true);

            session::put('result_request',$get_response['data']);
            session::save();
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
    }


    function reqResult() {
        $http       = new Client();
        $user       = Session::get('user');
        $result     = Session::get('result_request');

        if (!empty($result)) {
            return $result;
        }

    }

}
