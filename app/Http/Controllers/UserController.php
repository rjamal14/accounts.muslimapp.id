<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->apitmp   = "http://13.59.66.165:6000";
    }

    public function index(Request $request)
    {
        $ses = Session::get('user');
        return $ses;
    }

    public function showEdit()
    {
        $ses = Session::get('user');

        if ($ses['profile_picture'] == 'https://cloud.muslimapp.id/public/user/') {
            $ses['profile_picture'] = './images/skleton.png';
        }

        return $ses;
    }
    
    public function base64profile(Request $request)
    {
        $input      = $request->all();
        // $replace    = str_replace("data:image/jpeg;base64,","",$input[0]);
        
        Session::put('base64profile',$input[0]);

        return $input[0];
    }

    public function updateUser(Request $request, $id) 
    {
        $http   = new Client();
        $ses    = Session::get('user');
        $image  = Session::get('base64profile');

        $password             = $request->input('password');
        $konfirmasipassword   = $request->input('konfirmasipassword');

        if (!empty($image)) {
            $postData['profile_picture']    = $image;
            $base                           = 'data:image/jpeg;base64,';
            $ses['profile_picture']         = $image;
        }
        
        if ($password != Null || $konfirmasipassword !== Null)
        {
            $request->validate([
                'nama' => 'required',
                'no_hp' => 'required|numeric',
                'email' => 'required',
                'alamat' => 'required',
                'jenis_kelamin' => 'required',
                'tgl_lahir'     => 'required',
                'password'           => 'required',
                'konfirmasipassword' => 'required|same:password' 
            ]);
            
            $postData['password'] = $password;
        } else {
            $request->validate([
                'nama' => 'required',
                'no_hp' => 'required|numeric',
                'email' => 'required',
                'alamat' => 'required',
                'jenis_kelamin' => 'required',
                'tgl_lahir'     => 'required',
            ]);
        }
       
        $postData['id']     = $id;
        $postData['nama']   = $request->input('nama');
        $postData['email']  = $request->input('email');
        $postData['alamat'] = $request->input('alamat');
        $postData['no_hp']  = $request->input('no_hp');
        $postData['jenis_kelamin']  = $request->input('jenis_kelamin');
        $postData['tgl_lahir']         = $request->input('tgl_lahir');

        
        $response = $http->request('POST', $this->hostapi.'/restapi/Profile/?id='.$id, [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/x-www-form-urlencoded',
                'Accept'        => 'application/json'
            ],
            'form_params' => $postData
        ]);

        $get_response       = json_decode((string) $response->getBody(), true);
        
        if ($get_response['status'] == 'success') {
            $user_arr       = array(
                'user_id'           => $ses['user_id'],
                'nama'              => $postData['nama'],
                'email'             => $postData['email'],
                'token'             => $ses['token'],
                'profile_picture'   => $ses['profile_picture'],
                'alamat'            => $postData['alamat'],
                'tgl_lahir'         => $postData['tgl_lahir'],
                'jenis_kelamin'     => $postData['jenis_kelamin'],
                'no_hp'             => $postData['no_hp'],
                'password'          => '',
                'konfirmasipassword' => ''
            );
            Session::put('user',$user_arr);
            return response(200);
        } else {
            return response(500);
        }

    }

    function savePin(Request $request)
    {
        $http            = new Client();
        $user            = Session::get('user');
        
        $pin             = $request->input('pin');
        $konfirmasipin   = $request->input('konfirmasipin');

        if ($pin != Null || $konfirmasipin !== Null)
        {
            $request->validate([
                'pin'           => 'required|max:4',
                'konfirmasipin' => 'required|same:pin|max:4' 
            ]);
            
            $postData['pin'] = $pin;
        }

        try {
            $response    = $http->post($this->devapi.'/Wallet/Pin?type=save', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
                'json' => [
                    'user_id'   => '9371',//$user['user_id'],
                    'pin'       => $postData['pin'],
                ]
            ]);
    
            $get_response       = json_decode((string) $response->getBody(), true);
            
            if ($get_response['status'] == 'error') {
                return 'error';
            } else if ($get_response['status'] == 'success') {
                return $get_response['data'];
            }

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return 'error 500';
            }
        }
    }

    function validatePIN(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        
        $pin        = $request->input('PIN');
        
        try {
            $response    = $http->post($this->devapi.'/Wallet/Pin?type=validate', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
                'json' => [
                    'user_id'   => '9371',//$user['user_id'],
                    'pin'       => $pin,
                ]
            ]);
    
            $get_response       = json_decode((string) $response->getBody(), true);
            
            if ($get_response['status'] == 'error') {
                return 'error';
            } else if ($get_response['status'] == 'success') {
                return $get_response['data'];
            }

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return 'error 500';
            }
        }
        
    }

}
