<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class TransferPayController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->hostapi2  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->proapi   = "https://pro.muslimapp.id";
    }

    public function saveTransfer(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        
        $amount     = $request->input('amount');
        $username   = $request->input('username');
        $orderid    = $request->input('orderID');
        
        try {
            $response    = $http->post($this->devapi.'/Wallet/Send', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
                'json' => [
                        'order_id'   => $orderid,
                        'sender_id'  => '9371',//$user['user_id'],
                        "to"         => $username,
                        "amount"     => $amount,
                        "type"       => "transfer_request"
                ]
            ]);
    
            $get_response       = json_decode((string) $response->getBody(), true);
            
            if ($get_response['status'] == 'error') {
                return 'error';
            } else if ($get_response['status'] == 'success') {
                Session::put('TransferPay', $get_response['data']);
                Session::save();
                return $get_response['data'];
            }

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return 'error 500';
            }
        }
    }

    public function decodeUsername(Request $request) 
    {
        $http       = new Client();
        
        $username   = $request->input('username');
        
        try {
            $response    = $http->post($this->devapi.'/Wallet/Decode', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
                'json' => [
                        'userid'    => $username,
                ]
            ]);
    
            $get_response       = json_decode((string) $response->getBody(), true);
            
            return $get_response['data']['user_id'];
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return 'error 500';
            }
        }
    }

    public function trfResult(Request $request)
    {
        $trf       = Session::get('TransferPay');
        
        return $trf;
    }

}
