<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\TransaksiGas;
use App\TransaksiKas;
use App\TransaksiLain;
use App\Transaction;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use App\DetailTransaksiGasMasuk;
use App\DetailTransaksiGasKeluar;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Akun;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->hostapi2  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->proapi   = "https://pro.muslimapp.id";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return TransaksiGas::paginate(10);
    }

    public function getProfile(Request $request){
      $http     = new Client();
      $ses = Session::get('user');
      $user_id    = $ses['user_id'];
     
      $response = $http->get($this->hostapi.'/restapi/Profile/?id='.$user_id, [
        'headers' => [
            'Authorization' => 'Basic d2ViMTp3ZWIx',
            'Content-Type'  => 'application/json'
        ],
      ]);
      
      $get_response  = json_decode((string) $response->getBody(), true);
      
      return $get_response;
    }

    public function getOrderBy(Request $request){
      $http     = new Client();
     
      if(strpos($request->query('id'), 'DONASI') !== false){
        $response = $http->get($this->hostapi.'/Donasi/Order/get_by?order_id='.$request->query('id'), [
          'headers' => [
              'Authorization' => 'Basic d2ViMTp3ZWIx',
              'Content-Type'  => 'application/json'
          ],
        ]);
        
        $get_response  = json_decode((string) $response->getBody(), true);
  
        return $get_response['data'][0];
      }elseif(strpos($request->query('id'), 'AQIQAH') !== false){
        $response = $http->get($this->hostapi.'/restapi/Aqiqah/Order_aqiqah?id='.$request->query('id'), [
          'headers' => [
              'Authorization' => 'Basic d2ViMTp3ZWIx',
              'Content-Type'  => 'application/json'
          ],
        ]);
        
        $get_response  = json_decode((string) $response->getBody(), true);
  
        return $get_response['data'];
      } elseif(strpos($request->query('id'), 'QURBAN') !== false){
        $response = $http->get($this->hostapi.'/restapi/Qurban/Order_qurban?id='.$request->query('id'), [
          'headers' => [
              'Authorization' => 'Basic d2ViMTp3ZWIx',
              'Content-Type'  => 'application/json'
          ],
        ]);
        
        $get_response  = json_decode((string) $response->getBody(), true);
  
        return $get_response['data'][0];
      } elseif(strpos($request->query('id'), 'PAY') !== false){
        $response = $http->get($this->devapi.'/Wallet/History?trx_id='.$request->query('id'), [
          'headers' => [
              'Authorization' => 'Basic d2ViMTp3ZWIx',
              'Content-Type'  => 'application/json'
          ],
        ]);
        
        $get_response  = json_decode((string) $response->getBody(), true);
  
        return $get_response['data'][0];
      } else {
        $response = $http->get($this->hostapi.'/Order/ListOrder/detail?id='.$request->query('id'), [
          'headers' => [
              'Authorization' => 'Basic d2ViMTp3ZWIx',
              'Content-Type'  => 'application/json'
          ],
        ]);
        
        $get_response  = json_decode((string) $response->getBody(), true);
  
        return $get_response['data'][0];
      }
    }

    public function getOrderTopupWallet(Request $request){
      $http     = new Client();

      $ses      = Session::get('user');
      
      $user_id  = $ses['user_id'];

      $response = $http->get($this->devapi.'/Wallet/History?user_id='.$user_id, [
        'headers' => [
            'Authorization' => 'Basic d2ViMTp3ZWIx',
            'Content-Type'  => 'application/json'
        ],
      ]);
     

      $get_response  = json_decode((string) $response->getBody(), true);
      
      $data = array();

      foreach($get_response['data'] as $key => $row){
        $date=date_create($row['order_date']);  
        switch ($row['order_status']) {
            case 0:
              $status='Order masuk';
              break;
            case 1:
              $status='Sudah Konfirmasi';
              break;
            case 2:
              $status='Sudah Bayar';
              break;
            case 3:
              $status='Pesanan diproses';
              break;
            case 4:
              $status='On Delivery';
              break;
            case 5:
              $status='Delivered';
              break;
            case 6:
              $status='Pesanan ditolak';
              break;
            case 7:
              $status='Selesai';
              break;
    
            default:
              $status="error";
              break;
          }

        $data[] = array(
          'order_id' => $row['order_id'],
          'order_date' => date_format($date, 'd F Y'),
          'total_nominal' => number_format($row['grand_total']),
          'order_status' =>  $status
        );
      }

       #get amount wallet
       $response_wallet = $http->request('GET', 'https://staging.muslimapp.id/Wallet/History', [
        'headers' => [
        'Authorization' => 'Basic d2ViMTp3ZWIx',
        ],
        'query' => [
        'user_id' => $user_id,
        ]
      ]);

      $get_response_wallet       = json_decode((string) $response_wallet->getBody(), true);

      if($get_response_wallet['status'] == 'success'){
        $datawallet = $get_response_wallet['data'];
        $total_amount = 0;
        foreach ($datawallet as $get) {
          if ($get['order_status'] == '0') {
              $total_amount += $get['grand_total'];
          }
        }
        Session::put('amount',$total_amount);
        Session::save();
      }
      
      return $data;
    }

    public function getOrderIslamic(Request $request){
      $http     = new Client();

      $ses = Session::get('user');
      
      $user_id    = $ses['user_id'];

      $response = $http->get($this->hostapi.'/Order/ListOrder/detail?user_id='.$user_id, [
        'headers' => [
            'Authorization' => 'Basic d2ViMTp3ZWIx',
            'Content-Type'  => 'application/json'
        ],
      ]);

      $get_response  = json_decode((string) $response->getBody(), true);
      
      $data = array();

      foreach($get_response['data'] as $key => $row){
        $date=date_create($row['order_date']);  
        switch ($row['status']) {
            case 0:
              $status='Order masuk';
              break;
            case 1:
              $status='Sudah Konfirmasi';
              break;
            case 2:
              $status='Sudah Bayar';
              break;
            case 3:
              $status='Pesanan diproses';
              break;
            case 4:
              $status='On Delivery';
              break;
            case 5:
              $status='Delivered';
              break;
            case 6:
              $status='Pesanan ditolak';
              break;
            case 7:
              $status='Selesai';
              break;
    
            default:
              $status="error";
              break;
          }

        $data[] = array(
          'order_id' => $row['order_id'],
          'order_date' => date_format($date, 'd F Y'),
          'total_nominal' => number_format($row['grand_total']),
          'order_status' =>  $status
        );
      }
      return $data;
    }

    function getOrderQurban(Request $request) {
      $http     = new Client();

      $ses = Session::get('user');
      $user_id    = $ses['user_id'];

      $response = $http->get($this->hostapi.'/restapi/Qurban/Order_qurban?type=list&user_id='.$user_id, [
        'headers' => [
            'Authorization' => 'Basic d2ViMTp3ZWIx',
            'Content-Type'  => 'application/json'
        ],
      ]);

      $get_response  = json_decode((string) $response->getBody(), true);
      
      $data = array();

      foreach($get_response['data'] as $key => $row){
        $date=date_create($row['tgl_pesan']);  
        switch ($row['status']) {
            case 0:
              $status='Order masuk';
              break;
            case 1:
              $status='Sudah Konfirmasi';
              break;
            case 2:
              $status='Sudah Bayar';
              break;
            case 3:
              $status='Pesanan diproses';
              break;
            case 4:
              $status='On Delivery';
              break;
            case 5:
              $status='Delivered';
              break;
            case 6:
              $status='Pesanan ditolak';
              break;
            case 7:
              $status='Selesai';
              break;
    
            default:
              $status="error";
              break;
          }

        $data[] = array(
          'order_id' => $row['id_order'],
          'order_date' => date_format($date, 'd F Y'),
          'total_nominal' => number_format($row['total_nominal']),
          'order_status' =>  $status
        );
      }
      return $data;
    }

    public function getOrderAqiqah(Request $request){
      $http     = new Client();

      $ses = Session::get('user');
      $user_id    = $ses['user_id'];

      $response = $http->get($this->hostapi.'/restapi/Aqiqah/Order_aqiqah?type=list&user_id='.$user_id, [
        'headers' => [
            'Authorization' => 'Basic d2ViMTp3ZWIx',
            'Content-Type'  => 'application/json'
        ],
      ]);

      $get_response  = json_decode((string) $response->getBody(), true);

      $data = array();

      foreach($get_response['data'] as $key => $row){
        $date=date_create($row['tgl_pesan']);  
        switch ($row['status']) {
            case 0:
              $status='Order masuk';
              break;
            case 1:
              $status='Sudah Konfirmasi';
              break;
            case 2:
              $status='Sudah Bayar';
              break;
            case 3:
              $status='Pesanan diproses';
              break;
            case 4:
              $status='On Delivery';
              break;
            case 5:
              $status='Delivered';
              break;
            case 6:
              $status='Pesanan ditolak';
              break;
            case 7:
              $status='Selesai';
              break;
    
            default:
              $status="error";
              break;
          }

        $data[] = array(
          'order_id' => $row['id_order'],
          'order_date' => date_format($date, 'd F Y'),
          'total_nominal' => number_format($row['grand_total']),
          'order_status' =>  $status
        );
      }
      return $data;
    }

    public function listTrx(Request $request){
        $http     = new Client();

        $ses = Session::get('user');

        $user_id    = $ses['user_id'];
        $nama       = $ses['nama'];


        $response = $http->get($this->hostapi.'/Donasi/Order/get_by?user_id='.$user_id, [
          'headers' => [
              'Authorization' => 'Basic d2ViMTp3ZWIx',
              'Content-Type'  => 'application/json'
          ],
        ]);
        
        $get_response  = json_decode((string) $response->getBody(), true);

        $data = array();
  
        foreach($get_response['data'] as $key => $row){
          switch ($row['order_status']) {
            case 0:
              $status='Order masuk';
              break;
            case 1:
              $status='Sudah Konfirmasi';
              break;
            case 2:
              $status='Sudah Bayar';
              break;
            case 7:
              $status='Selesai';
              break;
    
            default:
              $status="error";
              break;
          }
          $date=date_create($row['order_date']);
            $data[] = array(
              'order_id' => $row['order_id'],
              'order_date' => date_format($date, 'd F Y'),
              'total_nominal' => number_format($row['total_nominal']),
              'order_status' =>  $status
            );
          
        }

        return $data;
        // return $paginatedItems;
      
    }

}
