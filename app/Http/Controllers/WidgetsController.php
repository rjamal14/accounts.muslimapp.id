<?php

namespace App\Http\Controllers;

use App\Model\Web;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Pagination\LengthAwarePaginator;
// use Illuminate\Support\Facades\Auth;

class WidgetsController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->hostapi2  = "https://server1.muslimapp.id";
        $this->hostapi3  = "https://server2.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";

        header("Access-Control-Allow-Origin: *");
    }

    public function banner(Request $request)
    {
        $http       = new Client();
        header("Access-Control-Allow-Origin: *");
        
        try {
            $response    = $http->get($this->devapi.'/Banner/List', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
            ]);
    
            $get_response                           = json_decode((string) $response->getBody(), true);
            $get_response['data'][0]['user_agent']  = $_SERVER['HTTP_USER_AGENT'];
            $get_response['data'][0]['csrf_token']  = $request->session()->token();

            return $get_response['data'][0];
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
        
    }

    public function getHref(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        $userAgent      = $_SERVER['HTTP_USER_AGENT'];

        $linkandroid    = 'https://play.google.com/store/apps/details?id=com.dmg.muslimapp';
        $linkios        = 'https://apps.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636';

        return $linkandroid;
    }

    public function video(Request $request)
    {
        $http       = new Client();
        header("Access-Control-Allow-Origin: *");

        try {
            $response    = $http->get($this->devapi.'/Video/List', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
            ]);
    
            $get_response                           = json_decode((string) $response->getBody(), true);
            $get_response['data'][0]['user_agent']  = $_SERVER['HTTP_USER_AGENT'];
            $get_response['data'][0]['csrf_token']  = $request->session()->token();
            return $get_response['data'][0];
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
    }
    
}
