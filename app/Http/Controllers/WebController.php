<?php

namespace App\Http\Controllers;

use App\Model\Web;
use Illuminate\Http\Request;
use DB;
use App;
use App\User;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->apitmp   = "http://13.59.66.165:6000";
    }

    public function index()
    {
        echo 'test';
    }
    
    public function indexapp()
    {
        $http     = new Client();
        
        if (!empty($_GET['token'])) {
            $token = $_GET['token'];
            Session::put('token',$token);

            #get user with token
            try {
                $response_token = $http->request('GET', $this->hostapi.'/LoginJWT/check', [
                                'headers' => [
                                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                                    'Accept' => 'Application/json'
                                ],
                                'query' => [
                                    'token' => $token,
                                ]
                            ]);
                $get_responsetoken  = json_decode((string) $response_token->getBody(), true);
                $data_userid        = $get_responsetoken['data']['_id'];
                
                $response_profile = $http->get($this->hostapi.'/restapi/Profile/?id='.$data_userid, [
                    'headers' => [
                        'Authorization' => 'Basic d2ViMTp3ZWIx',
                        'Content-Type'  => 'application/json'
                    ],
                ]);
                
                $get_response_profile   = json_decode((string) $response_profile->getBody(), true);
                
                $user_arr       = array(
                    'user_id'           => $get_response_profile['data']['id'],
                    'nama'              => $get_response_profile['data']['nama'],
                    'email'             => $get_response_profile['data']['email'],
                    'token'             => $get_response_profile['data']['jwt_token'],
                    'profile_picture'   => $get_response_profile['data']['profile_picture'],
                    'alamat'            => $get_response_profile['data']['alamat'],
                    'tgl_lahir'         => $get_response_profile['data']['tgl_lahir'],
                    'jenis_kelamin'     => $get_response_profile['data']['jenis_kelamin'],
                    'no_hp'             => $get_response_profile['data']['no_hp'],
                    'password'          => '',
                    'konfirmasipassword' => ''
                );
    
                Session::put('user',$user_arr);
    
            } catch (RequestException $e) {
                $arr1 = Psr7\str($e->getRequest());
                
                if ($e->hasResponse()) {
                    $arr2 = Psr7\str($e->getResponse());
                    return redirect('401');
                }
            }

        } else {
            $token = '';
        }
        
        return redirect('/');
    }


    public function LogoutToken(Request $request,$token) {
        $http   = new Client();

        if (!empty($token)) {
            Session::forget('user');
            $data['token'] = $token;
        } else {
            $data['token'] = '';
        }

       return redirect()->back();
    }

    public function log_out(Request $request) {
        $http   = new Client();
        $user   = Session::get('user');

        try {
            $response = $http->request('POST', $this->hostapi.'/LogoutJWT', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Accept' => 'Application/json'
                ],
                'json' => [
                    'user_id' => $user['user_id'],
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);

            Session::forget('user');
            Session::forget('base64profile');
            Session::forget('amount');
            Session::save();

            return redirect('https://donasi.muslimapp.id/logouttoken/'.$user['token']);
            return redirect('https://store.muslimapp.id/logouttoken/'.$user['token']);

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());

            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }


        return redirect('/');
    }

    public function log_in(Request $request)
    {
        $http     = new Client();

        try {
            $response = $http->request('POST', $this->hostapi.'/LoginJWT', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type' => 'application/json',
                    'Accept' => 'Application/json'
                ],
                'json' => [
                    'email' => $request->input('email'),
                    'password' => $request->input('password'),
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);
            if($get_response['status'] == 'error'){
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }

            $response_profile = $http->get($this->hostapi.'/restapi/Profile/?id='.$get_response['data'][0]['id'], [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'application/json'
                ],
            ]);

            $get_response_profile   = json_decode((string) $response_profile->getBody(), true);

            $user_arr       = array(
                'user_id'           => $get_response_profile['data']['id'],
                'nama'              => $get_response_profile['data']['nama'],
                'email'             => $get_response_profile['data']['email'],
                'token'             => $get_response_profile['data']['jwt_token'],
                'profile_picture'   => $get_response_profile['data']['profile_picture'],
                'alamat'            => $get_response_profile['data']['alamat'],
                'tgl_lahir'         => $get_response_profile['data']['tgl_lahir'],
                'jenis_kelamin'     => $get_response_profile['data']['jenis_kelamin'],
                'no_hp'             => $get_response_profile['data']['no_hp'],
                'password'          => '',
                'konfirmasipassword' => ''
            );

            Session::put('user',$user_arr);

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());

            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
        
        #get amount wallet
        $response_wallet = $http->request('GET', $this->hostapi.'/Wallet/History', [
            'headers' => [
            'Authorization' => 'Basic d2ViMTp3ZWIx',
            ],
            'query' => [
            'user_id' => $user_arr['user_id'],
            ]
        ]);

        $get_response_wallet       = json_decode((string) $response_wallet->getBody(), true);

        if ($get_response_wallet['status'] == 'success') {
            $datawallet = $get_response_wallet['data'];
            $total_amount = 0;
            foreach ($datawallet as $get) {
                if ($get['order_status'] == '0') {
                    $total_amount += $get['grand_total'];
                }
            }
            Session::put('amount',$total_amount);
            Session::save();
        }

        return redirect('/');
    }

    public function regis(Request $request) {
        $http     = new Client();

        try {
            $response = $http->request('POST', $this->hostapi.'/restapi/Register', [
                            'headers' => [
                                'Authorization' => 'Basic d2ViMTp3ZWIx',
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Accept' => 'Application/json'
                            ],
                            'form_params' => [
                                'nama' => $request->input('name'),
                                'email' => $request->input('email'),
                                'password' => $request->input('password'),
                                'no_hp'     => '00',
                                'alamat'     => 'jl',
                                'tgl_lahir'     => '1999-09-09'
                            ]
                        ]);
            $get_response       = json_decode((string) $response->getBody(), true);

            $user_arr       = array(
                'user_id'   => $get_response['iduser'],
                'nama'      => $request->input('name'),
                'email'     => $request->input('email'),
                'alamat'    => 'jl',
                'password'  => $request->input('password'),
            );

            Session::put('user',$user_arr);

        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());

            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email Sudah Digunakan sebelumnya', 'The Message']);
            }
        }

        return redirect('/home');
    }

}
