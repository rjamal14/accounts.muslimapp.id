<?php

namespace App\Http\Controllers;

use App\Model\Web;
use Illuminate\Http\Request;
use DB;
use App;
use App\User;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse; 
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class TopupController extends Controller
{
    public function __construct()
    {
        $this->hostapi  = "https://server1.muslimapp.id";
        $this->devapi   = "https://staging.muslimapp.id";
        $this->apitmp   = "http://13.59.66.165:6000";
    }
    
    public function transfer()
    {
        $http   = new Client();

        #get bank
        $response    = $http->get($this->hostapi.'/restapi/Bank/List_bank', [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);

        $get_bank   = json_decode((string) $response->getBody(), true);
        
        #memisahkan berdasarkan type
        foreach ($get_bank['data'] as $bank) {
            $allbank['bank'][$bank['type']]['id'][]          = $bank['id'];
            $allbank['bank'][$bank['type']]['nama_bank'][]   = $bank['nama_bank'];
        }
        
        return $allbank;
    }

    function savetransferVA(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        
        $saldo      = $request->input('saldo');
        $bank_id    = $request->input('bank_id');
        
        try {
            $response = $http->request('POST', $this->devapi.'/Wallet/Topup', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'Application/json',
                    'Accept'        => 'Application/json'
                ],
                'json' => [
                    'user_id'   => $user['user_id'],
                    'amount'    => $saldo,
                    'device_id' => 'PC',
                    'bank_id'   => $bank_id,
                    'type'      => 'topup',
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);
            
            $order['order_id']  = $get_response['order_id'];
            $order['bank_id']   = $bank_id;
            $order['saldo']     = $saldo;
            Session::put('order_id_va',$order);
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
    }

    function rinciantransferVA(Request $request)
    {
        $http       = new Client();
        
        $user       = Session::get('user');
        $order      = Session::get('order_id_va');

        $response    = $http->get($this->hostapi.'/restapi/Bank/List_bank?id='.$order['bank_id'], [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);

        $get_bank     = json_decode((string) $response->getBody(), true);
        
        Session::put('bankva',$get_bank['data'][0]);
        $bank       = Session::get('bankva');

        $data['bank']       = $bank;
        $data['user_id']    = $user['user_id'];
        $data['order']      = $order;
        
        echo json_encode($data);
    }

    function savetransferBank(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        
        $saldo      = $request->input('saldo');
        $bank_id    = $request->input('bank_id');
        
        try {
            $response = $http->request('POST', $this->devapi.'/Wallet/Topup', [
                'headers' => [
                    'Authorization' => 'Basic d2ViMTp3ZWIx',
                    'Content-Type'  => 'Application/json',
                    'Accept'        => 'Application/json'
                ],
                'json' => [
                    'user_id'   => $user['user_id'],
                    'amount'    => $saldo,
                    'device_id' => 'PC',
                    'bank_id'   => $bank_id,
                    'type'      => 'topup',
                ]
            ]);

            $get_response       = json_decode((string) $response->getBody(), true);
            
            $order['order_id']  = $get_response['order_id'];
            $order['bank_id']   = $bank_id;
            $order['saldo']     = $saldo;
            Session::put('order_id_trf',$order);
        } catch (RequestException $e) {
            $arr1 = Psr7\str($e->getRequest());
            
            if ($e->hasResponse()) {
                $arr2 = Psr7\str($e->getResponse());
                return redirect()->back()->withErrors(['Email atau Password Salah', 'The Message']);
            }
        }
    }

    function rinciantransferBank(Request $request)
    {
        $http       = new Client();
        $user       = Session::get('user');
        $order      = Session::get('order_id_trf');
        
        #get bank
        $response    = $http->get($this->hostapi.'/restapi/Bank/List_bank?id='.$order['bank_id'], [
            'headers' => [
                'Authorization' => 'Basic d2ViMTp3ZWIx',
                'Content-Type'  => 'application/json'
            ],
        ]);

        $get_bank     = json_decode((string) $response->getBody(), true);
        
        Session::put('bank',$get_bank['data'][0]);
        $bank       = Session::get('bank');

        $data['bank']       = $bank;
        $data['user_id']    = $user['user_id'];
        $data['order']      = $order;
        
        echo json_encode($data);
    }

}