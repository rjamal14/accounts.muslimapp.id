<?php

Route::get('/', function () {
        $user = Session::get('user');

        if (!empty($user)) {
                return redirect('/home#/transaction');
        } else {
                return view('auth.login');
        }
});

Auth::routes();

Route::post('/register','WebController@regis')->name('register');
Route::get('/p/logout','WebController@log_out')->name('log_out');
Route::get('/p','WebController@index')->name('index');
Route::post('/p/login','WebController@log_in')->name('log_in');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/index', 'UserController@index');
Route::get('/user/showEdit', 'UserController@showEdit');
Route::patch('/user/{id}/edit','UserController@updateUser');
Route::patch('/user/base64profile','UserController@base64profile');
Route::get('logouttoken/{id}','WebController@LogoutToken')->name('LogoutToken');

Route::get('/transaction/view', 'TransactionController@listTrx')->name('transaction');
Route::get('/transaction/get_profile', 'TransactionController@getProfile');
Route::get('/transaction/get_order_by', 'TransactionController@getOrderBy');
Route::get('/transaction/view/aqiqah', 'TransactionController@getOrderAqiqah');
Route::get('/transaction/view/islamic', 'TransactionController@getOrderIslamic');
Route::get('/transaction/view/topupwallet', 'TransactionController@getOrderTopupWallet');

Route::get('/topup/transferbank', 'TopupController@transfer');
Route::patch('/topup/sv/transferbank', 'TopupController@savetransferBank');
Route::get('/topup/rincian/transferbank', 'TopupController@rinciantransferBank');
Route::patch('/topup/sv/transferva', 'TopupController@savetransferVA');
Route::get('/topup/rincian/transferva', 'TopupController@rinciantransferVA');

Route::patch('/p/sv/request', 'RequestController@saveRequest');
Route::get('/request/result', 'RequestController@reqResult');

