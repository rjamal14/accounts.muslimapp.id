<!DOCTYPE html>
<html lang="zxx" class="js">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Muslimapp.id | Aplikasi jadwal Sholat, Aqiqah, Qurban, Donasi, Muslim Marketplace di Indonesia">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Fav Icon browser atas  -->
		<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
		<!-- Judul Site  -->
		<title>My Muslimapp Account - Satu Akun untuk semua fitur</title>
		<!-- Vendor Bundle CSS -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('css/vendor.bundle.css') }}">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('css/theme.css') }}" rel="stylesheet">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">

		<link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('css/element-index.css') }}">
		<link href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" rel="stylesheet">

		<style>
            .navbar-nav > li > a {
                font-size: 12px;
                margin: 0 5px;
            }
            .navbar-nav > li > a {
                background: transparent;
                text-transform: uppercase;
                color: inherit;
                font-family: "Open-Sans", sans-serif;
                font-weight: 500;
                line-height: 20px;
                padding: 14px 0;
                font-size: 12px;
                margin: 0 5px;
                transition: background 350ms, color 350ms, border-color 350ms;
            }
        </style>
	</head>
	<body style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif">
		<div id="app">
			<!-- Header -->
			<header class="site-header header-s1 is-sticky">
				<!-- Topbar -->
				<div class="topbar">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								<ul class="social">
									<li><a href="https://www.facebook.com/muslimapp.id" ><em alt="Facebook" class="fa fa-facebook"></em></a></li>
									<li><a href="https://www.twitter.com/muslimapp_id" ><em alt="Twitter" class="fa fa-twitter"></em></a></li>
									<li><a href="https://www.linkedin.com/company/muslimapp/" ><em alt="Linkedin" class="fa fa-linkedin"></em></a></li>
									<li><a href="https://www.instagram.com/muslimapp.id/" ><em alt="Instagram" class="fa fa-instagram"></em></a></li>
									<li><a href="https://api.whatsapp.com/send?phone=628112333724" target="_blank"><em alt="Support" class="fa fa-phone"></em></a></li>
								</ul>
							</div>
							<div class="col-sm-5 al-right" id="app-navbar-collapse">
									@if (Session::get('user'))
									<header-component></header-component>
									@endif
							</div>
						</div>
					</div>
				</div>
				<!-- #end Topbar -->
			</header>
			<br/>
			<div class="container">
				<div class="row">
					<div class="col-sm-4 ">
						<a href="{{url('/')}}">
							<img title="muslimapp.id logo" src="{{ asset('images/logon.png') }}">
						</a>
					</div>
					<!--div class="col-sm-8 font-weight-black" style="text-align:right">
						@if (Session::get('user'))
						<a href="javascript:;" style="color:black;font-size:25px">
							<?php $amount = Session::get('amount') ?>
							@if (!empty($amount))
								Rp. {{ number_format($amount,0,",",".") }}
							@else 
								Rp. 0
							@endif
							<img title="Wallet" src="{{ asset('icons@/wallet.png') }}">
						</a>
						@endif
					</div-->
				</div>
			</div>
			<!-- Section -->
			<div class="section section-pad">
				@yield('content')
			</div>
			<!-- End Section -->

			<!-- Section Footer -->
			<div class="footer-section section section-pad-md light bg-footer">
				<div class="imagebg footerbg">
					<img src="{{ asset('images/footer-bg.png') }}" alt="footer-bg">
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 wgs-box res-m-bttm">
							<!-- Each Widget -->
							<div class="wgs-footer wgs-menu">
								<h5 class="wgs-title ucap">Services</h5>
								<div class="wgs-content">
									<ul class="menu">
										<li><a href="https://donasi.muslimapp.id" target="_blank">Donasi</a></li>
										<li><a href="https://aqiqah.muslimapp.id" target="_blank">Aqiqah</a></li>
										<li><a href="https://qurban.muslimapp.id" target="_blank">Qurban</a></li>
										<li><a href="https://store.muslimapp.id" target="_blank">Islamic Product</a></li>
										<li><a href="https://booking.muslimapp.id" target="_blank">Booking Service Haji & Umroh</a></li>
									</ul>
								</div>
							</div>
							<!-- End Widget -->
						</div>
						<div class="col-md-4 col-sm-4 wgs-box res-m-bttm">
							<!-- Each Widget -->
							<div class="wgs-footer wgs-menu">
								<h5 class="wgs-title ucap">Information</h5>
								<div class="wgs-content">
									<ul class="menu">
										<li><a href="https://muslimapp.id/syaratdanketentuan/">Syarat & Ketentuan</a></li>
										<li><a href="https://muslimapp.id/kebijakanprivasi/">Kebijakan & Privasi</a></li>
									</ul>
								</div>
							</div>
							<!-- End Widget -->
						</div>
						<div class="col-md-4 col-sm-4 wgs-box res-m-bttm">
							<!-- Each Widget -->
							<div class="wgs-footer wgs-contact">
								<h5 class="wgs-title ucap">get in touch</h5>
								<div class="wgs-content">
									<ul class="wgs-contact-list">
										<li><span class="pe pe-7s-map-marker"></span>Wisma Monex<br/>Asia Afrika Bandung</li>
										<li><span class="pe pe-7s-call"></span>Telephone : <a href="https://api.whatsapp.com/send?phone=628112333724" target="_blank">+62 811 2333 724</a> <br/></li>
										<li><span class="pe pe-7s-global"></span>Email : <a href="mailto:info@muslimapp.id">info@muslimapp.id</a> <br/>Web : <a href="https://muslimapp.id">www.muslimapp.id</a></li>
									</ul>
								</div>
							</div>
							<!-- End Widget -->
						</div>
					</div>
				</div>
			</div>
			<!-- End Section -->

			<!-- Copyright -->
			<div class="copyright light">
				<div class="container">
					<div class="row">
						<div class="site-copy col-sm-7">
							<p>Copyright &copy; <?=date("Y")?> Muslimapp.id. Hak Cipta Oleh <a href="https://muslimapp.id">Muslimapp.id</a></p>
						</div>
						<div class="col-sm-5 text-right mobile-left">
							<ul class="social">
								<li><a href="https://www.facebook.com/muslimapp.id"><em class="fa fa-facebook"></em></a></li>
								<li><a href="https://www.twitter.com/muslimapp_id"><em class="fa fa-twitter"></em></a></li>
								<li><a href="https://www.linkedin.com/company/muslimapp/"><em class="fa fa-linkedin"></em></a></li>
								<li><a href="https://www.instagram.com/muslimapp.id/"><em class="fa fa-instagram"></em></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- End Copyright -->

			<!-- Preloader !remove please if you do not want -->
			<div id="preloader"><div id="status">&nbsp;</div></div>
			<!-- Preloader End -->
		</div>
		<!-- JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="{{ asset('js/app.js?v=1.0.2') }}"></script>
		<script src="{{ asset('js/jquery.bundle.js') }}"></script>
		<script src="{{ asset('js/script.js') }}"></script>
		<script src="{{ asset('js/jquery.ajax-cross-origin.min.js') }}"></script>
		<script src="{{ asset('js/accounting/accounting.js') }}"></script>
		<script src="{{ asset('js/accounting/accounting.min.js') }}"></script>

		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  		<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>

		<script src="{{ asset('js/element-index.js') }}"></script>

		</body>
	</body>
</html>
