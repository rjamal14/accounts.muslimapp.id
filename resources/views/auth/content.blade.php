<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <h3 class="heading-lead center">Login Your Account</h3>
        
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            @if($errors->any())
            <h4>{{$errors->first()}}</h4>
            @endif
            <div class="form-results"></div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="form-field form-m-bttm">
                    <input name="email" type="email" id="email" placeholder="Email *" class="form-control required email" aria-required="true" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="form-field">
                    <input name="password" type="password" id="password" placeholder="Password *" class="form-control required" aria-required="true" required>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-alt">Log In</button>
            <span class="gaps"></span>
            <p class="small">Not registered? 
                <a class="switch-tab" data-tabnav="loginreg-form" href="#tab2" data-toggle="tab">Register here</a>
            </p>
        </form>
    </div>
</div>