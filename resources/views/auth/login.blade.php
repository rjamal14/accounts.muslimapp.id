@extends('layouts.app')

@section('content')
<div class="container">
    <div class="tab-custom">
        <div class="row">
            <div class="col-md-4 col-md-offset-4  col-sm-6 col-sm-offset-3">
                <ul class="nav nav-tabs ucap" id="loginreg-form">
                    <li class="active"><a href="#tab1" data-toggle="tab">Log In</a></li>
                    <li><a href="#tab2" data-toggle="tab">Register</a></li>
                </ul>
            </div>
        </div>
        <div class="gaps size-2x"></div>
        <!-- Tab panes -->

        <div class="tab-content no-pd">
            <div class="tab-pane fade in active" id="tab1">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <h3 class="heading-lead center">Login Your Account</h3>
                        
                        <form method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            @if($errors->any())
                                <h4>{{$errors->first()}}</h4>
                            @endif
                            <div class="form-results"></div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="form-field form-m-bttm">
                                    <input name="email" type="email" id="email" placeholder="Email *" class="form-control required email" aria-required="true" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="form-field">
                                    <input name="password" type="password" id="password" placeholder="Password *" class="form-control required" aria-required="true" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-alt">Log In</button>
                            <span class="gaps"></span>
                            <p class="small">Not registered? 
                                <a class="switch-tab" data-tabnav="loginreg-form" href="#tab2" data-toggle="tab">Register here</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="tab2">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <h3 class="heading-lead center">Register An Account</h3>
                        <form name="form-signup" class="form-signup" action="{{ route('register') }}" method="post">
                        {{ csrf_field() }}

                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                            <div class="form-results"></div>
                            
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="form-field form-m-bttm">
                                    <input name="name" type="text" placeholder="Name *" class="form-control required" aria-required="true">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="form-field form-m-bttm">
                                    <input name="email" type="email" placeholder="Email *" class="form-control required email" aria-required="true">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="form-field">
                                    <input name="password" type="password" placeholder="Password *" class="form-control required" aria-required="true">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-field">
                                    <input name="password_confirmation" type="password" placeholder="Confirm Password *" class="form-control required" aria-required="true">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-alt">Signup</button>
                            <span class="gaps"></span>
                            <p class="small">Already registered? 
                                <a class="switch-tab" data-tabnav="loginreg-form" href="#tab1" data-toggle="tab">
                                Login here
                                </a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	
@endsection
