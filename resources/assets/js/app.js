
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import VueSwal from 'vue-swal';
import Vue from 'vue';
import Vuetify from 'vuetify'
import routes from './router.js';
import Selectize from 'vue2-selectize';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import WebCam from 'vue-web-cam';

import { ClientTable } from 'vue-tables-2';
import VModal from 'vue-js-modal';
import vueNumeralFilterInstaller from 'vue-numeral-filter';
import HeaderComponent from  './components/header.vue';
import VueQrcodeReader from 'vue-qrcode-reader'


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('vue-pagination', require('laravel-vue-pagination'));
Vue.component('vue-spinner', require('vue-simple-spinner'));
Vue.component('vue-selectize', Selectize);
Vue.component('header-component', require('./components/header.vue'));
Vue.component('webcam-component', WebCam)

Vue.use(VueSwal);
Vue.use(Vuetify);
Vue.use(VueRouter);

Vue.use(VModal);
Vue.use(ElementUI);
Vue.use(ClientTable);
Vue.use(vueNumeralFilterInstaller);
Vue.use(WebCam)

Vue.use(VueQrcodeReader)

const router = new VueRouter({
  routes 
});

const app = new Vue({
  router,
  components: { 
    HeaderComponent,
    'qrcode-stream': VueQrcodeReader.QrcodeStream,
    'qrcode-drop-zone': VueQrcodeReader.QrcodeDropZone,
    'qrcode-capture': VueQrcodeReader.QrcodeCapture
  }
}).$mount('#app');