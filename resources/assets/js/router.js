
// 1. Define route components.
// These can be imported from other files
// import DashboardIndex from './components/dashboard/DashboardIndex.vue';
import UserIndex from './components/user/UserIndex.vue';
import UserEdit from './components/user/UserEdit.vue';
import TransactionIndex from './components/transaction/TransactionIndex.vue';
import TopupIndex from './components/topup/TopupIndex.vue';
import TransferBankIndex from './components/topup/TransferBankIndex.vue';
import TransferVAIndex from './components/topup/TransferVAIndex.vue';
import KonfirmasiBank from './components/topup/KonfirmasiBank.vue';
import KonfirmasiVA from './components/topup/KonfirmasiVA.vue';
import TransferPay from './components/transferpay/TransferPayIndex.vue';
import RequestPay from './components/request/RequestIndex.vue';
import header from './components/header.vue';
import ResultRequest from './components/request/ResultRequest.vue';
import TransferScanQRResult from './components/transferpay/ResultScanQR.vue';
import settingpinIndex from './components/user/SettingPinIndex.vue';

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  // { path: '/', component: DashboardIndex,name:'IndexDashboard' },
  { path: '/user', component: UserIndex,name: 'IndexUser' },
  { path: '/user/edit/:id', component: UserEdit,name: 'EditUser' },
  { path: '/transaction', component:TransactionIndex,name: 'IndexTransaction' },
  { path: '/Topup', component:TopupIndex,name: 'IndexTopup' },
  { path: '/Topup/TransferBank', component:TransferBankIndex,name: 'IndexTransferBank' },
  { path: '/Topup/TransferVA', component:TransferVAIndex,name: 'IndexTransferVA' },
  { path: '/Topup/KonfirmasiBank', component:KonfirmasiBank,name: 'IndexKonfirmasiBank' },
  { path: '/Topup/KonfirmasiVA', component:KonfirmasiVA,name: 'IndexKonfirmasiVA' },
  { path: '/transferpay', component:TransferPay,name: 'IndexTransferPay' },
  { path: '/request', component:RequestPay,name: 'IndexRequest' },
  { path: '/request/Result', component:ResultRequest,name: 'IndexResultRequest' },
  { path: '/transfer/pay/scan/Result', component:TransferScanQRResult,name: 'IndexResultScanQR' },
  { path: '/header', component:header,name: 'Indexheader' },
  { path: '/p/logout',name: 'IndexLogout' },
  { path: '/p/user/settingpin',component:settingpinIndex, name: 'IndexSettingPin' },
]

export default routes;
